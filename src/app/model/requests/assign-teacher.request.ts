export class AssignTeacherRequest {

  teacherId: string;
  groupId: string;

  constructor(teacherId: string, groupId: string) {
    this.teacherId = teacherId;
    this.groupId = groupId;
  }
}
