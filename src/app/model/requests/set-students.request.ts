export class SetStudentsRequest {

  studentIds: string[];

  constructor(studentIds: string[]) {
    this.studentIds = studentIds;
  }
}
