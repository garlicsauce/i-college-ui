export class Student {

  public id: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public countryOfOrigin: string;
  public visaNumber: string;
  public passportNumber: string;
  public expanded: boolean;

  constructor(firstName: string,
              lastName: string,
              email: string,
              countryOfOrigin: string,
              visaNumber: string,
              passportNumber: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.countryOfOrigin = countryOfOrigin;
    this.visaNumber = visaNumber;
    this.passportNumber = passportNumber;
    this.expanded = false;
  }
}
