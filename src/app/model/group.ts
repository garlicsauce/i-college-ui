import {Teacher} from "./teacher";
import {Student} from "./student";

export class Group {

  public id: string;
  public symbol: string;
  public teacher: Teacher | undefined;
  public members: Student[];

  constructor(symbol: string,
              teacher: Teacher | undefined) {
    this.symbol = symbol;
    this.teacher = teacher;
  }
}
