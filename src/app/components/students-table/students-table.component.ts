import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Student} from "../../model/student";

@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styleUrls: ['./students-table.component.css']
})
export class StudentsTableComponent {

  constructor(
    public dialogRef: MatDialogRef<StudentsTableComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Student[]) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
