import {Component, OnInit} from '@angular/core';
import {StudentsService} from "../../services/students.service";
import {Student} from "../../model/student";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {MatTableDataSource} from "@angular/material/table";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class StudentsListComponent implements OnInit {

  headerElements = ['First name', 'Last name', 'Email'];
  students: MatTableDataSource<Student>;
  expandedStudent: Student | null;

  uploadForm: FormGroup;

  backendHost: string;

  constructor(private studentsService: StudentsService,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) {
    this.backendHost = environment.backendUrl;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.students.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    this.studentsService.fetchAllStudents()
      .subscribe((data: Student[]) => {
        this.students = new MatTableDataSource(data);
      });

    this.uploadForm = this.formBuilder.group({
      avatar: ['']
    });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('avatar').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('avatar', this.uploadForm.get('avatar').value);
    this.studentsService.uploadAvatar(this.expandedStudent.id, formData)
      .subscribe(() => {
        this.ngOnInit();
        this.toastr.success('Avatar uploaded successfully');
      });
  }
}
