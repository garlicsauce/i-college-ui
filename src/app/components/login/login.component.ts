import {Component} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {Role} from "../../model/role";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  Role = Role;

  constructor(private router: Router,
              private authService: AuthService) {
  }

  login(role: Role) {
    this.authService.login(role);
    this.router.navigate(['/']);
  }

}
