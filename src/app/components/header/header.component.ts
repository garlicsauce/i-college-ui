import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private router: Router,
              private authService: AuthService) { }

  get isAuthorized(): boolean {
    return this.authService.isAuthorized();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['']);
  }
}
