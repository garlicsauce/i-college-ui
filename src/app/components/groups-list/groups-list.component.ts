import {Component, OnInit} from '@angular/core';
import {GroupsService} from "../../services/groups.service";
import {Group} from "../../model/group";
import {MatTableDataSource} from "@angular/material/table";
import {TeachersService} from "../../services/teachers.service";
import {Teacher} from "../../model/teacher";
import {AssignTeacherRequest} from "../../model/requests/assign-teacher.request";
import {ToastrService} from "ngx-toastr";
import {MatDialog} from "@angular/material/dialog";
import {StudentsTableComponent} from "../students-table/students-table.component";

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.css']
})
export class GroupsListComponent implements OnInit {

  headerElements = ['Symbol', 'Teacher', 'Students'];
  groups: MatTableDataSource<Group>;
  teachers: Teacher[];

  constructor(private groupsService: GroupsService,
              private teachersService: TeachersService,
              private toastr: ToastrService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.groupsService.fetchAllGroups()
      .subscribe((data: Group[]) => {
        this.groups = new MatTableDataSource(data);
      })
    this.teachersService.fetchAllTeachers()
      .subscribe((data: Teacher[]) => {
        this.teachers = data;
      })
  }

  compareByTeacherId(teacher, otherTeacher) {
    return teacher.id === otherTeacher.id;
  }

  assignTeacher(group: Group): void {
    const assignTeacherRequest: AssignTeacherRequest =
      new AssignTeacherRequest(group.teacher.id, group.id);
    this.groupsService.assignTeacher(assignTeacherRequest)
      .subscribe(() => {
        this.toastr.success('Teacher ' + group.teacher.firstName + ' ' + group.teacher.lastName
          + ' has been assigned to a group ' + group.symbol);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.groups.filter = filterValue.trim().toLowerCase();
  }

  openDialog(group: Group): void {
    const dialogRef = this.dialog.open(StudentsTableComponent, {
      width: '250px',
      data: group.members
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed');
      });
  }
}
