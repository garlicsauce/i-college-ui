import {Component, OnInit} from '@angular/core';
import {StudentsService} from "../../services/students.service";
import {Student} from "../../model/student";
import {Group} from "../../model/group";
import {GroupsService} from "../../services/groups.service";
import {MatListOption} from "@angular/material/list";
import {FormControl, Validators} from "@angular/forms";
import {SetStudentsRequest} from "../../model/requests/set-students.request";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-assign-students',
  templateUrl: './assign-students.component.html',
  styleUrls: ['./assign-students.component.css']
})
export class AssignStudentsComponent implements OnInit {

  groupControl = new FormControl('', Validators.required);

  groups: Array<Group>;
  selectedGroup: Group;
  students: Array<Student>;
  studentsToBeAssigned: Array<Student>;

  constructor(private studentsService: StudentsService,
              private groupsService: GroupsService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.studentsService.fetchAllStudents()
      .subscribe((data: Student[]) => {
        this.students = data;
        this.studentsToBeAssigned = [];
      });
    this.groupsService.fetchAllGroups()
      .subscribe((data: Group[]) => {
        this.groups = data;
      });
  }

  studentChosen(selectedStudents) {
    this.studentsToBeAssigned = [];
    for (let selectedStudent of selectedStudents) {
      this.studentsToBeAssigned.push(selectedStudent.value);
    }
  }

  onSubmit() {
    let setStudentsRequest: SetStudentsRequest = new SetStudentsRequest(this.studentsToBeAssigned.map(s => s.id));
    console.log(setStudentsRequest);
    this.groupsService.setStudents(this.selectedGroup.id, setStudentsRequest)
      .subscribe(() => {
        this.toastr.success('Students were assigned to a group');
      });
  }
}
