import {Component, OnInit} from '@angular/core';
import {Announcement} from "../../model/announcement";
import {AnnouncementsService} from "../../services/announcements.service";

@Component({
  selector: 'app-announcements-list',
  templateUrl: './announcements-list.component.html',
  styleUrls: ['./announcements-list.component.css']
})
export class AnnouncementsListComponent implements OnInit {

  announcements: Array<Announcement>;

  constructor(private announcementsService: AnnouncementsService) {
  }

  ngOnInit(): void {
    this.announcementsService.fetchAllAnnouncements()
      .subscribe((data: Announcement[]) => {
        this.announcements = data;
      });
  }

}
