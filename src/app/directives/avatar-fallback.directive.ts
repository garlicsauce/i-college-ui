import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: 'img[avatarFallback]'
})
export class AvatarFallbackDirective {

  constructor(private eRef: ElementRef) { }

  @HostListener('error')
  loadFallbackOnError() {
    const element: HTMLImageElement = <HTMLImageElement>this.eRef.nativeElement;
    element.src = 'assets/images/fallbackAvatar.png';
  }
}
