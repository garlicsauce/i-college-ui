import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {MDBBootstrapModule} from "angular-bootstrap-md";
import {RegistrationFormComponent} from './forms/registration/registration-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {StudentsListComponent} from './components/students-list/students-list.component';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from './components/home/home.component';
import {AnnouncementsListComponent} from './components/announcements-list/announcements-list.component';
import {AnnouncementComponent} from './components/announcement/announcement.component';
import {AddAnnouncementComponent} from "./forms/add-announcement/add-announcement.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {AvatarFallbackDirective} from './directives/avatar-fallback.directive';
import {ToastrModule} from "ngx-toastr";
import {AddGroupComponent} from './forms/add-group/add-group.component';
import {GroupsListComponent} from './components/groups-list/groups-list.component';
import {AddTeacherComponent} from './forms/add-teacher/add-teacher.component';
import {MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {AssignStudentsComponent} from './components/assign-students/assign-students.component';
import {MatListModule} from "@angular/material/list";
import {MatDialogModule} from "@angular/material/dialog";
import {StudentsTableComponent} from './components/students-table/students-table.component';
import {AuthService} from "./services/auth.service";
import {LoginComponent} from './components/login/login.component';
import {UserRoleDirective} from './directives/user-role.directive';
import {UserDirective} from './directives/user.directive';
import {MatButtonModule} from "@angular/material/button";
import {AuthGuard} from "./guards/auth.guard";

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'registration', canActivate: [AuthGuard], component: RegistrationFormComponent },
  { path: 'all-students', canActivate: [AuthGuard], component: StudentsListComponent },
  { path: 'add-announcement', canActivate: [AuthGuard], component: AddAnnouncementComponent },
  { path: 'add-group', canActivate: [AuthGuard], component: AddGroupComponent },
  { path: 'all-groups', canActivate: [AuthGuard], component: GroupsListComponent },
  { path: 'add-teacher', canActivate: [AuthGuard], component: AddTeacherComponent },
  { path: 'assign-students', canActivate: [AuthGuard], component: AssignStudentsComponent },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegistrationFormComponent,
    StudentsListComponent,
    HomeComponent,
    AnnouncementsListComponent,
    AnnouncementComponent,
    AddAnnouncementComponent,
    AvatarFallbackDirective,
    AddGroupComponent,
    GroupsListComponent,
    AddTeacherComponent,
    AssignStudentsComponent,
    StudentsTableComponent,
    LoginComponent,
    UserRoleDirective,
    UserDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatListModule,
    MatTableModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // for debugging
    ),
    ToastrModule.forRoot(),
  ],
  providers: [AuthGuard, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
