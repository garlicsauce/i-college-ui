import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Teacher} from "../model/teacher";
import {Observable, throwError} from "rxjs";
import {environment} from "../../environments/environment";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  addTeacher(teacher: Teacher): Observable<any> {
    console.log('Adding a teacher');
    return this.http
      .post(environment.backendUrl + '/teachers', JSON.stringify(teacher), this.httpOptions)
      .pipe(catchError(err => TeachersService.handleError(err)));
  }

  fetchAllTeachers(): Observable<Teacher[]> {
    console.log('Fetching all teachers');
    return this.http
      .get<Teacher[]>(environment.backendUrl + '/teachers', this.httpOptions);
  }

  private static handleError(error: HttpErrorResponse): Observable<Object> {
    console.log(error.message);
    return throwError('Unable to perform the action');
  }

}
