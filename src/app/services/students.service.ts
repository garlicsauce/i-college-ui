import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Student} from "../model/student";
import {catchError} from "rxjs/operators";
import {Observable, throwError} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private http: HttpClient) {}

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  registerStudent(student: Student): Observable<any> {
    console.log('Registering a student');
    return this.http
      .post(environment.backendUrl + '/students', JSON.stringify(student), this.httpOptions)
      .pipe(catchError(err => StudentsService.handleError(err)));
  }

  fetchAllStudents(): Observable<Student[]> {
    console.log('Fetching all students');
    return this.http
      .get<Student[]>(environment.backendUrl + '/students', this.httpOptions);
  }

  uploadAvatar(studentId: string, formData: FormData): Observable<any> {
    console.log('Uploading avatar for student id ' + studentId);

    return this.http
      .put(environment.backendUrl + '/students/' + studentId + '/photos/avatar', formData)
      .pipe(catchError(err => StudentsService.handleError(err)));
  }

  fetchAvatar(studentId: string): Observable<Blob> {
    console.log('Fetching avatar for student id ' + studentId);
    return this.http
      .get(environment.backendUrl + '/students/' + studentId + '/photos/avatar', { responseType: 'blob' });
  }

  private static handleError(error: HttpErrorResponse): Observable<Object> {
    console.log(error.message);
    return throwError('Unable to perform the action');
  }
}
