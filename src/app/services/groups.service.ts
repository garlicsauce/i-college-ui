import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Group} from "../model/group";
import {environment} from "../../environments/environment";
import {catchError} from "rxjs/operators";
import {AssignTeacherRequest} from "../model/requests/assign-teacher.request";
import {SetStudentsRequest} from "../model/requests/set-students.request";
import set = Reflect.set;

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  constructor(private http: HttpClient) {}

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  createGroup(group: Group): Observable<any> {
    console.log('Adding a group');
    return this.http
      .post(environment.backendUrl + '/groups', JSON.stringify(group), this.httpOptions)
      .pipe(catchError(err => GroupsService.handleError(err)));
  }

  fetchAllGroups(): Observable<Group[]> {
    console.log('Fetching all groups');
    return this.http
      .get<Group[]>(environment.backendUrl + '/groups', this.httpOptions)
  }

  assignTeacher(assignTeacherRequest: AssignTeacherRequest): Observable<any> {
    console.log('Assigning teacher to a group');
    return this.http
      .put(environment.backendUrl + '/groups/' + assignTeacherRequest.groupId + '/teachers/main',
        JSON.stringify(assignTeacherRequest), this.httpOptions)
      .pipe(catchError(err => GroupsService.handleError(err)));
  }

  setStudents(groupId: string, setStudentsRequest: SetStudentsRequest): Observable<any> {
    console.log('Setting students of a group');
    return this.http
      .put(environment.backendUrl + '/groups/' + groupId + '/students', JSON.stringify(setStudentsRequest), this.httpOptions)
      .pipe(catchError(err => GroupsService.handleError(err)));
  }

  private static handleError(error: HttpErrorResponse): Observable<Object> {
    console.log(error.message);
    return throwError('Unable to perform the action');
  }
}
