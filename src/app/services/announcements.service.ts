import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Announcement} from "../model/announcement";
import {environment} from "../../environments/environment";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AnnouncementsService {

  constructor(private http: HttpClient) {}

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  addAnnouncement(announcement: Announcement): Observable<any> {
    console.log('Creating an announcement');
    return this.http
      .post(environment.backendUrl + '/announcements', JSON.stringify(announcement), this.httpOptions)
      .pipe(catchError(err => AnnouncementsService.handleError(err)));
  }

  fetchAllAnnouncements(): Observable<Announcement[]> {
    console.log('Fetching all announcements');
    return this.http
      .get<Announcement[]>(environment.backendUrl + '/announcements', this.httpOptions);
  }

  private static handleError(error: HttpErrorResponse): Observable<Object> {
    console.log(error.message);
    return throwError('Unable to perform the action');
  }
}
