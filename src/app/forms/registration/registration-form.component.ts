import {Component} from '@angular/core';
import {Student} from "../../model/student";
import {StudentsService} from "../../services/students.service";

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent {

  student = RegistrationFormComponent.newStudent();

  constructor(private studentsService: StudentsService) {}

  onSubmit() {
    console.log(this.diagnostic);
    this.studentsService.registerStudent(this.student)
      .subscribe(() => {
        this.student = RegistrationFormComponent.newStudent();
      });
  }

  get diagnostic() {
    return JSON.stringify(this.student);
  }

  private static newStudent(): Student {
    return new Student('', '', '', '', '', '');
  }
}
