import {Component} from '@angular/core';
import {Group} from "../../model/group";
import {GroupsService} from "../../services/groups.service";

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})
export class AddGroupComponent {

  group = AddGroupComponent.newGroup();

  constructor(private groupsService: GroupsService) { }

  onSubmit() {
    console.log(this.diagnostic);
    this.groupsService.createGroup(this.group)
      .subscribe(() => {
        this.group = AddGroupComponent.newGroup();
      });
  }

  get diagnostic() {
    return JSON.stringify(this.group);
  }

  private static newGroup(): Group {
    return new Group('', null);
  }

}
