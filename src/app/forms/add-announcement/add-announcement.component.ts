import {Component} from '@angular/core';
import {Announcement} from "../../model/announcement";
import {AnnouncementsService} from "../../services/announcements.service";

@Component({
  selector: 'app-add-announcement',
  templateUrl: './add-announcement.component.html',
  styleUrls: ['./add-announcement.component.css']
})
export class AddAnnouncementComponent {

  announcement = AddAnnouncementComponent.newAnnouncement();

  constructor(private announcementsService: AnnouncementsService) { }

  onSubmit() {
    console.log(this.diagnostic);
    this.announcementsService.addAnnouncement(this.announcement)
      .subscribe(() => {
        this.announcement = AddAnnouncementComponent.newAnnouncement();
      });
  }

  get diagnostic() {
    return JSON.stringify(this.announcement);
  }

  private static newAnnouncement(): Announcement {
    return new Announcement('', '');
  }
}
