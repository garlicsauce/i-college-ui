import {Component} from '@angular/core';
import {Teacher} from "../../model/teacher";
import {TeachersService} from "../../services/teachers.service";

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})
export class AddTeacherComponent {

  teacher = AddTeacherComponent.newTeacher();

  constructor(private teachersService: TeachersService) { }

  onSubmit() {
    console.log(this.diagnostic);
    this.teachersService.addTeacher(this.teacher)
      .subscribe(() => {
        this.teacher = AddTeacherComponent.newTeacher();
      });
  }

  get diagnostic() {
    return JSON.stringify(this.teacher);
  }

  private static newTeacher(): Teacher {
    return new Teacher('', '', '');
  }

}
